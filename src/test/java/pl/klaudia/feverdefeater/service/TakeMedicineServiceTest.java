package pl.klaudia.feverdefeater.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.repository.TakeMedicineRepository;
import pl.klaudia.feverdefeater.repository.UserRepository;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.mapper.TakeMedicineMapper;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TakeMedicineServiceTest {
    private MedicineRepository medicineRepository = Mockito.mock(MedicineRepository.class);
    private UserRepository userRepository = Mockito.mock(UserRepository.class);

    private TakeMedicineMapper takeMedicineMapper = new TakeMedicineMapper(medicineRepository, userRepository);
    private  TakeMedicineRepository takeMedicineRepository = Mockito.mock(TakeMedicineRepository.class);


    private TakeMedicineService takeMedicineService = new TakeMedicineService(takeMedicineMapper, takeMedicineRepository);

    @Test
    public void getAll() {
        List<TakeMedicine> takeMedicineList = new ArrayList<>();

        when(takeMedicineRepository.findAll()).thenReturn(takeMedicineList);

        List<TakeMedicineDto> takeMedicineDtoList = takeMedicineService.getAll();

        assertEquals(takeMedicineList.size(), takeMedicineDtoList.size());

    }

    @Test
    public void findTakeMedicineById() throws NotFound {

        TakeMedicine takeMedicine
                = TakeMedicine.builder()
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineDoseMl(5)
                .medicineId(1)
                .createdAt(OffsetDateTime.now())
                .id(2)
                .timeOfNextDose(LocalDateTime.now().plusHours(6))
                .build();

        when(takeMedicineRepository.findById(2)).thenReturn(Optional.of(takeMedicine));

        TakeMedicineDto result = takeMedicineService.findTakeMedicineById(2);

        assertEquals(result.getId(), takeMedicine.getId());
        assertEquals(result.getTimeWhenTookMedicine(), takeMedicine.getTimeWhenTookMedicine());
        assertEquals(result.getMedicineId(), takeMedicine.getMedicineId());
        assertEquals(result.getMedicineDoseMl(), takeMedicine.getMedicineDoseMl(), 0);
        assertEquals(result.getTimeOfNextDose(), takeMedicine.getTimeOfNextDose());
    }

    @Test(expected = NotFound.class)
    public void findTakeMedicineById_shouldThrowNotFound() throws NotFound {

        TakeMedicineDto result = takeMedicineService.findTakeMedicineById(25);

    }

    @Test
    public void deleteTakeMedicine() throws NotFound {

        TakeMedicine takeMedicine
                = TakeMedicine.builder()
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineDoseMl(5)
                .medicineId(1)
                .createdAt(OffsetDateTime.now())
                .id(2)
                .timeOfNextDose(LocalDateTime.now().plusHours(6))
                .build();

        when(takeMedicineRepository.findById(2)).thenReturn(Optional.of(takeMedicine));

        TakeMedicineDto takeMedicineDto = takeMedicineService.deleteTakeMedicine(2);

        assertEquals(takeMedicineDto.getId(), takeMedicine.getId());
        assertEquals(takeMedicineDto.getMedicineId(), takeMedicine.getMedicineId());
        assertEquals(takeMedicineDto.getTimeWhenTookMedicine(), takeMedicine.getTimeWhenTookMedicine());

    }
}