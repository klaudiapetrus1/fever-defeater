package pl.klaudia.feverdefeater.service;

import org.junit.Test;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.model.TakeMedicine;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class DoseTimerDurationCalculatorTest {
    private DoseTimerDurationCalculator doseTimerDurationCalculator = new DoseTimerDurationCalculator();

    @Test
    public void getDuration() {
        TakeMedicineDto takeMedicineDtoTest1
                = TakeMedicineDto.builder()
                .id(1)
                .timeOfNextDose(LocalDateTime.now().plusHours(6).minusMinutes(6).minusSeconds(6))
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineId(1)
                .medicineDoseMl(4)
                .build();

        TakeMedicineDto takeMedicineDtoTest2
                = TakeMedicineDto.builder()
                .id(1)
                .timeOfNextDose(LocalDateTime.now().plusMinutes(6))
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineId(1)
                .medicineDoseMl(4)
                .build();

        TakeMedicineDto takeMedicineDtoTest3
                = TakeMedicineDto.builder()
                .id(1)
                .timeOfNextDose(LocalDateTime.now().plusHours(1))
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineId(1)
                .medicineDoseMl(4)
                .build();

        TakeMedicineDto takeMedicineDtoTest4
                = TakeMedicineDto.builder()
                .id(1)
                .timeOfNextDose(LocalDateTime.now().plusHours(22).plusMinutes(25).plusSeconds(02))
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineId(1)
                .medicineDoseMl(4)
                .build();

        TakeMedicineDto takeMedicineDtoTest5
                = TakeMedicineDto.builder()
                .id(1)
                .timeOfNextDose(LocalDateTime.now())
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineId(1)
                .medicineDoseMl(4)
                .build();

        String resultString1 = doseTimerDurationCalculator.getDuration(takeMedicineDtoTest1);
        String resultString2 = doseTimerDurationCalculator.getDuration(takeMedicineDtoTest2);
        String resultString3 = doseTimerDurationCalculator.getDuration(takeMedicineDtoTest3);
        String resultString4 = doseTimerDurationCalculator.getDuration(takeMedicineDtoTest4);
        String resultString5 = doseTimerDurationCalculator.getDuration(takeMedicineDtoTest5);

        assertEquals(resultString1, "05:53:54");
        assertEquals(resultString2, "00:06:00");
        assertEquals(resultString3, "01:00:00");
        assertEquals(resultString4, "22:25:02");
        assertEquals(resultString5, "00:00:00");
    }
}