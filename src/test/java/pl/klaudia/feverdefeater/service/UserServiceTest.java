package pl.klaudia.feverdefeater.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.klaudia.feverdefeater.controller.dto.UserDto;
import pl.klaudia.feverdefeater.model.User;
import pl.klaudia.feverdefeater.repository.UserRepository;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.service.mapper.UserDtoMapper;
import pl.klaudia.feverdefeater.view.dto.UserEditDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private UserRepository userRepository = Mockito.mock(UserRepository.class);
    private UserDtoMapper userDtoMapper = Mockito.spy(UserDtoMapper.class);
    private PasswordEncoder passwordEncoder = Mockito.mock(PasswordEncoder.class);

    private UserService userService = new UserService(userRepository, userDtoMapper, passwordEncoder);

    @Test
    public void getUsers() {
        List<User> userList = new ArrayList<>();

        when(userRepository.findAll()).thenReturn(userList);

        List<UserDto> resultList = userService.getUsers();

        assertEquals(userList.size(), resultList.size());
    }

    @Test
    public void getUserById() throws UserNotFound {

        User user
                = User.builder()
                .id(1)
                .tookMedicineList(new ArrayList<>())
                .role("USER")
                .password("qwerty")
                .login("jarek12")
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        UserDto userDto = userService.getUserById(1);

        assertEquals(user.getId(), userDto.getId());
        assertEquals(user.getLogin(), userDto.getLogin());
    }

    @Test(expected = UserNotFound.class)
    public void getUserById_shouldThrowUserNotFound() throws UserNotFound {
        userService.getUserById(727);
    }

    @Test
    public void editUser() throws UserNotFound {
        UserEditDto userEditDto = UserEditDto.builder().id(1).login("Olga").role("USER").build();
        User user
                = User.builder()
                .login("Robert")
                .id(1)
                .role("ADMIN")
                .tookMedicineList(new ArrayList<>())
                .build();

        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);

        UserDto userDto = userService.editUser(userEditDto);

        assertEquals(userDto.getLogin(), userEditDto.getLogin());
        assertEquals(userDto.getRole(), userEditDto.getRole());
        assertEquals(userDto.getId(), userEditDto.getId());
    }

    @Test
    public void deleteUser() throws UserNotFound {

        User user
                = User.builder()
                .login("Robert")
                .id(1)
                .role("ADMIN")
                .tookMedicineList(new ArrayList<>())
                .build();

        when(userRepository.findById(1)).thenReturn(Optional.of(user));

        UserDto deletedUserDto = userService.deleteUser(1);

        assertEquals(user.getId(), deletedUserDto.getId());
        assertEquals(user.getLogin(), deletedUserDto.getLogin());

    }

    @Test(expected = UserNotFound.class)
    public void deleteUser_shouldThrowUserNotFound() throws UserNotFound {
        userService.deleteUser(72);
    }
}