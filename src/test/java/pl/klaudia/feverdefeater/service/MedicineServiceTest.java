package pl.klaudia.feverdefeater.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.klaudia.feverdefeater.controller.dto.CreateMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.MedicineDto;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.Substance;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.service.exception.InvalidData;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.mapper.MedicineMapper;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MedicineServiceTest {

    private MedicineRepository medicineRepository = mock(MedicineRepository.class);
    private MedicineMapper medicineMapper = spy(MedicineMapper.class);

    private MedicineService medicineService = new MedicineService(medicineRepository, medicineMapper);

    @Test
    public void getAllMedicines() {
        List<Medicine> medicineList = new ArrayList<>();

        when(medicineRepository.findAll()).thenReturn(medicineList);

        List<MedicineDto> medicineDtoList = medicineService.getAllMedicines();

        assertEquals(medicineList.size(), medicineDtoList.size());

    }

    @Test
    public void findMedicineById() throws InvalidData {
        Medicine medicine = Medicine.builder()
                .createdAt(OffsetDateTime.now())
                .substance(Substance.PARACETAMOL)
                .id(1)
                .mgPerOneMl(20)
                .name("paracetamol")
                .build();

        when(medicineRepository.findById(1)).thenReturn(Optional.of(medicine));

        MedicineDto resultMedicineDto = medicineService.findMedicineById(1);

        assertEquals(resultMedicineDto.getId(), medicine.getId());
        assertEquals(resultMedicineDto.getName(), medicine.getName());
        assertEquals(resultMedicineDto.getMgPerOneMl(), medicine.getMgPerOneMl(), 0);
        assertEquals(resultMedicineDto.getSubstance(), medicine.getSubstance());
    }


    @Test(expected = InvalidData.class)
    public void findMedicineById_shouldThrowInvalidDataException() throws InvalidData {

        MedicineDto resultMedicineDto = medicineService.findMedicineById(1);

    }

    @Test
    public void deleteMedicineById() throws InvalidData {
        Medicine medicine = Medicine.builder()
                .createdAt(OffsetDateTime.now())
                .substance(Substance.PARACETAMOL)
                .id(1)
                .mgPerOneMl(20)
                .name("paracetamol")
                .build();

        when(medicineRepository.findById(1)).thenReturn(Optional.of(medicine));

        MedicineDto deletedMedicineDto = medicineService.deleteMedicineById(1);

        assertEquals(deletedMedicineDto.getName(), medicine.getName());
        assertEquals(deletedMedicineDto.getId(), medicine.getId());
        assertEquals(deletedMedicineDto.getSubstance(), medicine.getSubstance());
        assertEquals(deletedMedicineDto.getMgPerOneMl(), medicine.getMgPerOneMl(), 0);
    }

    @Test(expected = InvalidData.class)
    public void deleteMedicineById_shouldThrowInvalidData() throws InvalidData {

        MedicineDto deletedMedicineDto = medicineService.deleteMedicineById(167);

    }

    @Test
    public void updateMedicine() throws InvalidData {
        CreateMedicineDto createMedicineDto
                = CreateMedicineDto.builder()
                .name("apap")
                .substance(Substance.PARACETAMOL)
                .mgPerOneMl(4)
                .build();

        Medicine medicine = Medicine.builder()
                .createdAt(OffsetDateTime.now())
                .substance(Substance.PARACETAMOL)
                .id(1)
                .mgPerOneMl(20)
                .name("paracetamol")
                .build();

        when(medicineRepository.findById(1)).thenReturn(Optional.of(medicine));
        when(medicineRepository.save(medicine)).thenReturn(medicine);

        MedicineDto medicineDto = medicineService.updateMedicine(1, createMedicineDto);

        assertEquals(medicineDto.getMgPerOneMl(), createMedicineDto.getMgPerOneMl(), 0);
        assertEquals(medicineDto.getName(), createMedicineDto.getName());
        assertEquals(medicineDto.getSubstance(), createMedicineDto.getSubstance());
    }
}