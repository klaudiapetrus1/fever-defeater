package pl.klaudia.feverdefeater.service.mapper;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.klaudia.feverdefeater.controller.dto.CreateUserDto;
import pl.klaudia.feverdefeater.controller.dto.UserDto;
import pl.klaudia.feverdefeater.model.User;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class UserDtoMapperTest {


    private UserDtoMapper userDtoMapper = new UserDtoMapper();

    @Test
    public void fromEntityToUserDto() {
        User user
                = User.builder()
                .id(1)
                .tookMedicineList(new ArrayList<>())
                .role("USER")
                .password("qwerty")
                .login("jarek12")
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();

        UserDto userDto = userDtoMapper.fromEntityToUserDto(user);

        assertEquals(userDto.getId(), user.getId());
        assertEquals(userDto.getLogin(), user.getLogin());
        assertEquals(userDto.getRole(), user.getRole());
        assertEquals(userDto.getCreatedAt(), user.getCreatedAt());
        assertEquals(userDto.getUpdatedAt(), user.getUpdatedAt());
        assertTrue(userDto.getTookMedicineIdsList().isEmpty());


    }

    @Test
    public void toEntity() {

        CreateUserDto createUserDto
                = CreateUserDto.builder()
                .login("olek17")
                .password("12345")
                .repeatedPassword("12345")
                .build();

        User user = userDtoMapper.toEntity(createUserDto);

        assertEquals(user.getPassword(), createUserDto.getPassword());
        assertNotNull(user.getCreatedAt());
        assertEquals(user.getLogin(), createUserDto.getLogin());
        assertEquals(user.getRole(), "USER");
    }
}