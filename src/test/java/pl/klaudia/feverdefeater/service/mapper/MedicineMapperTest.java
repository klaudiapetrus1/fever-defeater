package pl.klaudia.feverdefeater.service.mapper;

import org.junit.jupiter.api.Test;
import pl.klaudia.feverdefeater.controller.dto.CreateMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.MedicineDto;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.Substance;

import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.*;

class MedicineMapperTest {
    private MedicineMapper medicineMapper = new MedicineMapper();

    @Test
    void createMedicineDtoToModel() {
        CreateMedicineDto createMedicineDto
                = CreateMedicineDto.builder()
                .name("ibuprom")
                .substance(Substance.PARACETAMOL)
                .mgPerOneMl(4)
                .build();

        Medicine medicine = medicineMapper.createMedicineDtoToModel(createMedicineDto);

        assertEquals(createMedicineDto.getName(), medicine.getName());
        assertEquals(createMedicineDto.getSubstance(), medicine.getSubstance());
        assertEquals(createMedicineDto.getMgPerOneMl(), medicine.getMgPerOneMl());

    }

    @Test
    void modelToMedicineDto() {
        Medicine medicine
                = Medicine.builder()
                .name("Apap")
                .mgPerOneMl(2)
                .id(2)
                .substance(Substance.PARACETAMOL)
                .createdAt(OffsetDateTime.now())
                .build();

        MedicineDto medicineDto = medicineMapper.modelToMedicineDto(medicine);

        assertEquals(medicine.getMgPerOneMl(), medicineDto.getMgPerOneMl());
        assertEquals(medicine.getName(), medicineDto.getName());
        assertEquals(medicine.getSubstance(), medicineDto.getSubstance());
        assertEquals(medicine.getId(), medicineDto.getId());
    }
}