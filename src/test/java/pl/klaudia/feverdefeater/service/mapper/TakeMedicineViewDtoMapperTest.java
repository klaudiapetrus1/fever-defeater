package pl.klaudia.feverdefeater.service.mapper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.configuration.IMockitoConfiguration;
import org.mockito.junit.MockitoJUnitRunner;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.service.DoseTimerDurationCalculator;
import pl.klaudia.feverdefeater.view.dto.TakeMedicineViewDto;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class TakeMedicineViewDtoMapperTest {

private DoseTimerDurationCalculator doseTimerDurationCalculator = mock(DoseTimerDurationCalculator.class);

private TakeMedicineViewDtoMapper takeMedicineViewDtoMapper = new TakeMedicineViewDtoMapper(doseTimerDurationCalculator);

    @Test
    public void fromTakeMedicineDtoToView() {
        //given
        TakeMedicineDto takeMedicineDto
                = TakeMedicineDto.builder()
                .medicineDoseMl(5)
                .medicineId(2)
                .timeWhenTookMedicine(LocalDateTime.now())
                .id(1)
                .timeOfNextDose(LocalDateTime.now().plusHours(4))
                .build();
        //when
        TakeMedicineViewDto takeMedicineViewDto = takeMedicineViewDtoMapper.fromTakeMedicineDtoToView(takeMedicineDto);
        //then
        assertNotSame(takeMedicineViewDto.getTimeLeftToNextDose(), "");
        assertEquals(takeMedicineViewDto.getId(), takeMedicineDto.getId());
        assertEquals(takeMedicineViewDto.getMedicineId(), takeMedicineDto.getMedicineId());
        assertEquals(takeMedicineViewDto.getTimeWhenTookMedicine(), takeMedicineDto.getTimeWhenTookMedicine());
        assertEquals(takeMedicineViewDto.getTimeOfNextDose(), takeMedicineDto.getTimeOfNextDose());
        assertEquals(takeMedicineViewDto.getMedicineDoseMl(), takeMedicineDto.getMedicineDoseMl(), 0);
    }
}