package pl.klaudia.feverdefeater.service.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.Substance;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.view.dto.UsersTookMedicineDto;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserTookMedicineMapperTest {

    private MedicineRepository medicineRepository = Mockito.mock(MedicineRepository.class);

    private UserTookMedicineMapper userTookMedicineMapper = new UserTookMedicineMapper(medicineRepository);

    @Test
    public void toUsersTookMedicineDto() {
        //given
        TakeMedicine takeMedicine
                = TakeMedicine.builder()
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineDoseMl(5)
                .medicineId(1)
                .createdAt(OffsetDateTime.now())
                .id(2)
                .timeOfNextDose(LocalDateTime.now().plusHours(6))
                .build();

        Medicine medicine
                = Medicine.builder()
                .name("Apap")
                .mgPerOneMl(2)
                .id(2)
                .substance(Substance.PARACETAMOL)
                .createdAt(OffsetDateTime.now())
                .build();
        //when
        when(medicineRepository.findById(takeMedicine.getMedicineId())).thenReturn(Optional.of(medicine));
        UsersTookMedicineDto usersTookMedicineDto = userTookMedicineMapper.toUsersTookMedicineDto(takeMedicine);

        //then
        assertEquals(usersTookMedicineDto.getId(), takeMedicine.getId());
        assertEquals(usersTookMedicineDto.getMedicineDoseMl(), takeMedicine.getMedicineDoseMl(), 0);
        assertEquals(usersTookMedicineDto.getTimeWhenTookMedicine(), takeMedicine.getTimeWhenTookMedicine());
        assertEquals(usersTookMedicineDto.getTimeOfNextDose(), takeMedicine.getTimeOfNextDose());
        assertEquals(usersTookMedicineDto.getUser(), takeMedicine.getUser());
        assertEquals(usersTookMedicineDto.getCreatedAt(), takeMedicine.getCreatedAt());
        assertEquals(usersTookMedicineDto.getMedicineName(), medicine.getName());
    }

}