package pl.klaudia.feverdefeater.service.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.klaudia.feverdefeater.model.Child;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.Substance;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.view.dto.ChildDto;

import java.time.OffsetDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChildDtoMapperTest {

    private MedicineRepository medicineRepository =  mock(MedicineRepository.class);

    private ChildDtoMapper childDtoMapper = new ChildDtoMapper(medicineRepository);

    @Test
    public void toEntity() throws NotFound {
        //given
        ChildDto childDto = new ChildDto(10, 1);
        Medicine medicine = Medicine.builder()
                .createdAt(OffsetDateTime.now())
                .substance(Substance.PARACETAMOL)
                .id(1)
                .mgPerOneMl(20)
                .name("paracetamol")
                .build();
        //when
        when(medicineRepository.findById(childDto.getMedicineId())).thenReturn(Optional.of(medicine));
        Child child = childDtoMapper.toEntity(childDto);
        //then
        assertThat(child.getWeight()).isEqualTo(childDto.getWeight());
    }
}