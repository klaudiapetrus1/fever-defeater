package pl.klaudia.feverdefeater.service.mapper;

import org.junit.Test;
import pl.klaudia.feverdefeater.model.MedicineDoseResult;
import pl.klaudia.feverdefeater.model.Substance;
import pl.klaudia.feverdefeater.view.dto.ChildDto;
import pl.klaudia.feverdefeater.view.dto.MedicineApplicationDto;

import static org.junit.Assert.*;

public class MedicineApplicationMapperTest {
    private MedicineApplicationMapper medicineApplicationMapper = new MedicineApplicationMapper();

    @Test
    public void toDto() {
        ChildDto childDto
                = ChildDto.builder()
                .medicineId(1)
                .weight(8)
                .build();

        MedicineDoseResult medicineDoseResult
                = MedicineDoseResult.builder()
                .dailyMedicineDoseMl(60)
                .singleMedicineDoseMg(4)
                .numberOfDailyDoses(4)
                .dailySubstanceDoseMg(70)
                .singleMedicineDoseMl(50)
                .substance(Substance.PARACETAMOL)
                .build();

        MedicineApplicationDto medicineApplicationDto = medicineApplicationMapper.toDto(childDto, medicineDoseResult);

        assertEquals(medicineApplicationDto.getMedicineId(), childDto.getMedicineId());
        assertEquals(medicineApplicationDto.getWeight(), childDto.getWeight(), 0);
        assertEquals(medicineApplicationDto.getDailyMedicineDoseMl(), medicineDoseResult.getDailyMedicineDoseMl(), 0);
        assertEquals(medicineApplicationDto.getSingleMedicineDoseMg(), medicineDoseResult.getSingleMedicineDoseMg(), 0);
        assertEquals(medicineApplicationDto.getNumberOfDailyDoses(), medicineDoseResult.getNumberOfDailyDoses());
        assertEquals(medicineApplicationDto.getDailySubstanceDoseMg(), medicineDoseResult.getDailySubstanceDoseMg(), 0);
        assertEquals(medicineApplicationDto.getSingleMedicineDoseMl(), medicineDoseResult.getSingleMedicineDoseMl(), 0);


    }
}