package pl.klaudia.feverdefeater.service.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.klaudia.feverdefeater.controller.dto.CreateTakeMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.Substance;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.model.User;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.repository.UserRepository;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.view.dto.MedicineApplicationDto;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TakeMedicineMapperTest {

    private MedicineRepository medicineRepository = mock(MedicineRepository.class);

    private UserRepository userRepository = mock(UserRepository.class);

    private TakeMedicineMapper takeMedicineMapper = new TakeMedicineMapper(medicineRepository, userRepository);

    @Test
    public void fromEntityToDto() {

        TakeMedicine takeMedicine
                = TakeMedicine.builder()
                .timeWhenTookMedicine(LocalDateTime.now())
                .medicineDoseMl(5)
                .medicineId(1)
                .createdAt(OffsetDateTime.now())
                .id(2)
                .timeOfNextDose(LocalDateTime.now().plusHours(6))
                .build();

        TakeMedicineDto takeMedicineDto = takeMedicineMapper.fromEntityToDto(takeMedicine);

        assertEquals(takeMedicineDto.getId(), takeMedicine.getId());
        assertEquals(takeMedicineDto.getTimeOfNextDose(), takeMedicine.getTimeOfNextDose());
        assertEquals(takeMedicineDto.getMedicineDoseMl(), takeMedicine.getMedicineDoseMl(), 0);
        assertEquals(takeMedicineDto.getMedicineId(), takeMedicine.getMedicineId());
        assertEquals(takeMedicineDto.getTimeWhenTookMedicine(), takeMedicine.getTimeWhenTookMedicine());



    }

    @Test
    public void toEntityTakeMedicine() throws UserNotFound {
        //given
        CreateTakeMedicineDto createTakeMedicineDto
                = CreateTakeMedicineDto.builder()
                .medicineDoseMl(5)
                .medicineId(1)
                .timeWhenTookMedicine(LocalDateTime.now())
                .timeOfNextDose(LocalDateTime.now().plusHours(4))
                .usersId(1)
                .build();

        User user
                = User.builder()
                .createdAt(LocalDateTime.now())
                .id(1)
                .login("julek")
                .password("qwerty")
                .role("USER")
                .tookMedicineList(new ArrayList<>())
                .build();

        //when
        when(userRepository.findById(createTakeMedicineDto.getUsersId())).thenReturn(Optional.of(user));
        TakeMedicine takeMedicine = takeMedicineMapper.toEntityTakeMedicine(createTakeMedicineDto);

        //then
        assertThat(takeMedicine.getUser().getId()).isEqualTo(createTakeMedicineDto.getUsersId());
        assertThat(takeMedicine.getTimeOfNextDose()).isEqualTo(createTakeMedicineDto.getTimeOfNextDose());
        assertThat(takeMedicine.getTimeWhenTookMedicine()).isEqualTo(createTakeMedicineDto.getTimeWhenTookMedicine());
        assertThat(takeMedicine.getMedicineId()).isEqualTo(createTakeMedicineDto.getMedicineId());
        assertThat(takeMedicine.getMedicineDoseMl()).isEqualTo(createTakeMedicineDto.getMedicineDoseMl());

    }

    @Test
    public void fromMedicineApplicationDtoToCreateTakeMedicineDto_ParacetamolCase() throws NotFound {
        //given
        MedicineApplicationDto medicineApplicationDto
                = MedicineApplicationDto.builder()
                .dailyMedicineDoseMl(20)
                .medicineId(1)
                .singleMedicineDoseMg(5)
                .singleMedicineDoseMl(5)
                .dailySubstanceDoseMg(50)
                .numberOfDailyDoses(4)
                .weight(8)
                .build();

        Medicine medicine
                = Medicine.builder()
                .substance(Substance.PARACETAMOL)
                .id(1)
                .mgPerOneMl(2)
                .createdAt(OffsetDateTime.now())
                .name("Apap")
                .build();

        //when
        when(medicineRepository.findById(medicineApplicationDto.getMedicineId())).thenReturn(Optional.of(medicine));

        CreateTakeMedicineDto createTakeMedicineDto
                = takeMedicineMapper.fromMedicineApplicationDtoToCreateTakeMedicineDto(medicineApplicationDto);
        //then
        assertThat(createTakeMedicineDto.getMedicineId()).isEqualTo(medicineApplicationDto.getMedicineId());
        assertThat(createTakeMedicineDto.getMedicineDoseMl()).isEqualTo(medicineApplicationDto.getSingleMedicineDoseMl());
        assertThat(createTakeMedicineDto.getTimeOfNextDose()).isEqualTo(createTakeMedicineDto.getTimeWhenTookMedicine().plusHours(4));
    }

    @Test
    public void fromMedicineApplicationDtoToCreateTakeMedicineDto_IbuprofenCase() throws NotFound {
        //given
        MedicineApplicationDto medicineApplicationDto
                = MedicineApplicationDto.builder()
                .dailyMedicineDoseMl(20)
                .medicineId(1)
                .singleMedicineDoseMg(5)
                .singleMedicineDoseMl(5)
                .dailySubstanceDoseMg(50)
                .numberOfDailyDoses(4)
                .weight(8)
                .build();

        Medicine medicine
                = Medicine.builder()
                .substance(Substance.IBUPROFEN)
                .id(1)
                .mgPerOneMl(2)
                .createdAt(OffsetDateTime.now())
                .name("Ibuprom")
                .build();

        //when
        when(medicineRepository.findById(medicineApplicationDto.getMedicineId())).thenReturn(Optional.of(medicine));

        CreateTakeMedicineDto createTakeMedicineDto
                = takeMedicineMapper.fromMedicineApplicationDtoToCreateTakeMedicineDto(medicineApplicationDto);
        //then
        assertThat(createTakeMedicineDto.getMedicineId()).isEqualTo(medicineApplicationDto.getMedicineId());
        assertThat(createTakeMedicineDto.getMedicineDoseMl()).isEqualTo(medicineApplicationDto.getSingleMedicineDoseMl());
        assertThat(createTakeMedicineDto.getTimeOfNextDose()).isEqualTo(createTakeMedicineDto.getTimeWhenTookMedicine().plusHours(6));
    }
}