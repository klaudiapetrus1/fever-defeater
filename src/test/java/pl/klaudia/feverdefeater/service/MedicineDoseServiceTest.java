package pl.klaudia.feverdefeater.service;

import org.junit.Test;
import pl.klaudia.feverdefeater.model.Child;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.MedicineDoseResult;
import pl.klaudia.feverdefeater.model.Substance;

import java.time.OffsetDateTime;

import static org.junit.Assert.*;

public class MedicineDoseServiceTest {

    private MedicineDoseService medicineDoseService = new MedicineDoseService();

    @Test
    public void calculateMedicineDose_IbuprofenCase() {
        Medicine medicine
                = Medicine.builder()
                .name("Apap")
                .mgPerOneMl(2)
                .id(2)
                .substance(Substance.PARACETAMOL)
                .createdAt(OffsetDateTime.now())
                .build();
        Child child = Child.builder().medicine(medicine).weight(10).build();

        MedicineDoseResult medicineDoseResult = medicineDoseService.calculateMedicineDose(child);

        assertEquals(medicineDoseResult.getSubstance(), medicine.getSubstance());
        assertEquals(medicineDoseResult.getDailyMedicineDoseMl(),
                medicineDoseResult.getDailySubstanceDoseMg() / medicine.getMgPerOneMl(), 0);
        assertEquals(medicineDoseResult.getDailySubstanceDoseMg(), child.getWeight() * 90, 0);
        assertEquals(medicineDoseResult.getNumberOfDailyDoses(), 4);
        assertEquals(medicineDoseResult.getSingleMedicineDoseMg(), child.getWeight() * 15, 0);
        assertEquals(medicineDoseResult.getSingleMedicineDoseMl(),
                medicineDoseResult.getSingleMedicineDoseMg() / medicine.getMgPerOneMl(), 0);
    }

    @Test
    public void calculateMedicineDose_ParacetamolCase() {
        Medicine medicine
                = Medicine.builder()
                .name("Ibuprom")
                .mgPerOneMl(2)
                .id(2)
                .substance(Substance.IBUPROFEN)
                .createdAt(OffsetDateTime.now())
                .build();
        Child child = Child.builder().medicine(medicine).weight(10).build();

        MedicineDoseResult medicineDoseResult = medicineDoseService.calculateMedicineDose(child);

        assertEquals(medicineDoseResult.getSubstance(), medicine.getSubstance());
        assertEquals(medicineDoseResult.getDailyMedicineDoseMl(),
                medicineDoseResult.getDailySubstanceDoseMg() / medicine.getMgPerOneMl(), 0);
        assertEquals(medicineDoseResult.getDailySubstanceDoseMg(), child.getWeight() * 30, 0);
        assertEquals(medicineDoseResult.getNumberOfDailyDoses(), 3);
        assertEquals(medicineDoseResult.getSingleMedicineDoseMg(), child.getWeight() * 10, 0);
        assertEquals(medicineDoseResult.getSingleMedicineDoseMl(),
                medicineDoseResult.getSingleMedicineDoseMg() / medicine.getMgPerOneMl(), 0);
    }
}