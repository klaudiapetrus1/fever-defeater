package pl.klaudia.feverdefeater.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.klaudia.feverdefeater.controller.dto.CreateMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.MedicineDto;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.service.exception.InvalidData;
import pl.klaudia.feverdefeater.service.mapper.MedicineMapper;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MedicineService {

    private final MedicineRepository repository;
    private final MedicineMapper mapper;

    public MedicineDto addMedicine(CreateMedicineDto createMedicineDto){
        Medicine medicine = mapper.createMedicineDtoToModel(createMedicineDto);
        medicine.setCreatedAt(OffsetDateTime.now());
      repository.save(medicine);
      return mapper.modelToMedicineDto(medicine);
    }

    public List<MedicineDto> getAllMedicines(){
        return repository.findAll().stream()
                .map(medicine -> mapper.modelToMedicineDto(medicine))
                .collect(Collectors.toList());
    }

    public MedicineDto findMedicineById(int id) throws InvalidData {

        return mapper.modelToMedicineDto(repository.findById(id).orElseThrow(InvalidData::new));
    }

    public MedicineDto deleteMedicineById(int id) throws InvalidData {
        Medicine medicine = repository.findById(id).orElseThrow(InvalidData::new);
        repository.deleteById(id);
        return mapper.modelToMedicineDto(medicine);
    }

    public MedicineDto updateMedicine(int id, CreateMedicineDto createMedicineDto) throws InvalidData {
        Medicine medicine = repository.findById(id).orElseThrow(InvalidData::new);
        medicine.setName(createMedicineDto.getName());
        medicine.setSubstance(createMedicineDto.getSubstance());
        medicine.setMgPerOneMl(createMedicineDto.getMgPerOneMl());
        medicine.setUpdatedAt(OffsetDateTime.now());
        Medicine updated = repository.save(medicine);
        return mapper.modelToMedicineDto(updated);
    }


}
