package pl.klaudia.feverdefeater.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.klaudia.feverdefeater.controller.dto.CreateTakeMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.repository.TakeMedicineRepository;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.service.mapper.TakeMedicineMapper;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TakeMedicineService {
    private final TakeMedicineMapper takeMedicineMapper;
    private final TakeMedicineRepository takeMedicineRepository;

    public List<TakeMedicineDto> getAll(){
       return takeMedicineRepository.findAll().stream()
               .map(takeMedicine -> takeMedicineMapper.fromEntityToDto(takeMedicine))
               .collect(Collectors.toList());
    }

    public TakeMedicineDto createTakeMedicine(CreateTakeMedicineDto createTakeMedicineDto) throws UserNotFound {
        TakeMedicine takeMedicine = takeMedicineMapper.toEntityTakeMedicine(createTakeMedicineDto);
        takeMedicine.setCreatedAt(OffsetDateTime.now());
        TakeMedicine saved = takeMedicineRepository.save(takeMedicine);
        return takeMedicineMapper.fromEntityToDto(saved);
    }

    public TakeMedicineDto findTakeMedicineById(int id) throws NotFound {
        return takeMedicineMapper.fromEntityToDto(takeMedicineRepository.findById(id).orElseThrow(NotFound::new));
    }

    public TakeMedicineDto deleteTakeMedicine(int id) throws NotFound {
        TakeMedicineDto takeMedicineDto = findTakeMedicineById(id);
        takeMedicineRepository.deleteById(id);
        return takeMedicineDto;

    }

}
