package pl.klaudia.feverdefeater.service;

import org.springframework.stereotype.Service;
import pl.klaudia.feverdefeater.model.Child;
import pl.klaudia.feverdefeater.model.MedicineDoseResult;
import pl.klaudia.feverdefeater.model.Substance;

@Service
public class MedicineDoseService {

    //dawka paracetamolu 15mg/kg max 4 razy na dobe (90mg/kg)
    //dawka ibuprofenu 10mg/kg max 3 razy na dobe (30mg/kg)

    public MedicineDoseResult calculateMedicineDose(Child child){
         MedicineDoseResult medicineDoseResult = new MedicineDoseResult();
        double singleMedicineDoseMg = 0;
        double dailySubstanceDoseMg = 0;
        int numberOfDailyDoses;
        Substance substance = child.getMedicine().getSubstance();

        if(substance.equals(Substance.IBUPROFEN)){
            dailySubstanceDoseMg = child.getWeight() * 30;
            singleMedicineDoseMg = child.getWeight() * 10;
            numberOfDailyDoses = 3;

        } else {
            dailySubstanceDoseMg = child.getWeight() * 90;
            singleMedicineDoseMg = child.getWeight() * 15;
            numberOfDailyDoses = 4;

        }
        double singleMedicineDoseMl = singleMedicineDoseMg / child.getMedicine().getMgPerOneMl();
        double dailyMedicineDoseMl = dailySubstanceDoseMg / child.getMedicine().getMgPerOneMl();

        medicineDoseResult.setDailySubstanceDoseMg(dailySubstanceDoseMg);
        medicineDoseResult.setSingleMedicineDoseMg(singleMedicineDoseMg);
        medicineDoseResult.setNumberOfDailyDoses(numberOfDailyDoses);
        medicineDoseResult.setDailyMedicineDoseMl(dailyMedicineDoseMl);
        medicineDoseResult.setSingleMedicineDoseMl(singleMedicineDoseMl);
        medicineDoseResult.setSubstance(substance);

        return medicineDoseResult;
    }
}
