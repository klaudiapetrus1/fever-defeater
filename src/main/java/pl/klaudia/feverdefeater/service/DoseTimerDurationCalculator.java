package pl.klaudia.feverdefeater.service;
import org.springframework.stereotype.Component;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.model.TakeMedicine;

import java.time.Duration;
import java.time.LocalDateTime;

@Component
public class DoseTimerDurationCalculator {
    private String format(Duration d) {
        long hours = d.toHours();
        d = d.minusHours(hours);
        long minutes = d.toMinutes();
        d = d.minusMinutes(minutes);
        long seconds = d.getSeconds();
        hours = hours * -1;
        minutes = minutes * -1;
        seconds = seconds * -1;
        return (hours == 0 ? "00" : hours < 10 ? "0" + hours : hours) + ":" +
                (minutes ==  0 ? "00" : minutes < 10 ? "0" + minutes : minutes)+ ":" +
                (seconds == 0 ? "00" : seconds < 10 ? "0" + seconds : seconds);
    }


    public String getDuration(TakeMedicineDto takeMedicineDto){
        LocalDateTime from = takeMedicineDto.getTimeOfNextDose();
        LocalDateTime now = LocalDateTime.now();

        Duration duration = Duration.between(from, now);
        return
//                duration.getSeconds() * -1;
        format(duration);
    }


}
