package pl.klaudia.feverdefeater.service.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.service.DoseTimerDurationCalculator;
import pl.klaudia.feverdefeater.view.dto.TakeMedicineViewDto;

@Component
@RequiredArgsConstructor
public class TakeMedicineViewDtoMapper {
    public final DoseTimerDurationCalculator doseTimerDurationCalculator;

    public TakeMedicineViewDto fromTakeMedicineDtoToView(TakeMedicineDto takeMedicineDto){
        return TakeMedicineViewDto.builder()
                .id(takeMedicineDto.getId())
                .medicineDoseMl(takeMedicineDto.getMedicineDoseMl())
                .medicineId(takeMedicineDto.getMedicineId())
                .timeOfNextDose(takeMedicineDto.getTimeOfNextDose())
                .timeWhenTookMedicine(takeMedicineDto.getTimeWhenTookMedicine())
                .timeLeftToNextDose(doseTimerDurationCalculator.getDuration(takeMedicineDto))
                .build();
    }
}
