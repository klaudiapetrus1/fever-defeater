package pl.klaudia.feverdefeater.service.mapper;

import org.springframework.stereotype.Component;
import pl.klaudia.feverdefeater.model.MedicineDoseResult;
import pl.klaudia.feverdefeater.view.dto.ChildDto;
import pl.klaudia.feverdefeater.view.dto.MedicineApplicationDto;

@Component
public class MedicineApplicationMapper {
    public MedicineApplicationDto toDto(ChildDto childDto, MedicineDoseResult medicineDoseResult){
        return MedicineApplicationDto.builder()
                .medicineId(childDto.getMedicineId())
                .weight(childDto.getWeight())
                .dailyMedicineDoseMl(medicineDoseResult.getDailyMedicineDoseMl())
                .numberOfDailyDoses(medicineDoseResult.getNumberOfDailyDoses())
                .dailySubstanceDoseMg(medicineDoseResult.getDailySubstanceDoseMg())
                .singleMedicineDoseMg(medicineDoseResult.getSingleMedicineDoseMg())
                .singleMedicineDoseMl(medicineDoseResult.getSingleMedicineDoseMl())
                .build();
    }
}
