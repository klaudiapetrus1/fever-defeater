package pl.klaudia.feverdefeater.service.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.klaudia.feverdefeater.controller.dto.CreateMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.CreateTakeMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.Substance;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.model.User;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.repository.UserRepository;
import pl.klaudia.feverdefeater.service.MedicineService;
import pl.klaudia.feverdefeater.service.exception.InvalidData;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.view.dto.MedicineApplicationDto;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class TakeMedicineMapper {
    private final MedicineRepository medicineRepository;
    private final UserRepository userRepository;

    public TakeMedicineDto fromEntityToDto(TakeMedicine takeMedicine){
        return TakeMedicineDto.builder()
                .id(takeMedicine.getId())
                .medicineDoseMl(takeMedicine.getMedicineDoseMl())
                .medicineId(takeMedicine.getMedicineId())
                .timeOfNextDose(takeMedicine.getTimeOfNextDose())
                .timeWhenTookMedicine(takeMedicine.getTimeWhenTookMedicine())
                .build();
    }

    public TakeMedicine toEntityTakeMedicine(CreateTakeMedicineDto createTakeMedicineDto) throws UserNotFound {
        User user = createTakeMedicineDto.getUsersId() != null ? userRepository.findById(createTakeMedicineDto.getUsersId()).orElseThrow(UserNotFound::new) : null;
        return TakeMedicine.builder()
                .timeOfNextDose(createTakeMedicineDto.getTimeOfNextDose())
                .medicineId(createTakeMedicineDto.getMedicineId())
                .medicineDoseMl(createTakeMedicineDto.getMedicineDoseMl())
                .timeWhenTookMedicine(createTakeMedicineDto.getTimeWhenTookMedicine())
                .user(user)
                .build();
    }

    public CreateTakeMedicineDto fromMedicineApplicationDtoToCreateTakeMedicineDto(MedicineApplicationDto medicineApplicationDto) throws NotFound {
        Medicine medicine = medicineRepository.findById(medicineApplicationDto.getMedicineId()).orElseThrow(NotFound::new);
                LocalDateTime timeOfNextDose;
        if(medicine.getSubstance().equals(Substance.IBUPROFEN)){
         timeOfNextDose = LocalDateTime.now().plusHours(6);
        } else {
            timeOfNextDose = LocalDateTime.now().plusHours(4);
        }

        return CreateTakeMedicineDto.builder()
                .medicineId(medicineApplicationDto.getMedicineId())
                .medicineDoseMl(medicineApplicationDto.getSingleMedicineDoseMl())
                .timeOfNextDose(timeOfNextDose)
                .timeWhenTookMedicine(LocalDateTime.now())
                .build();
    }
}
