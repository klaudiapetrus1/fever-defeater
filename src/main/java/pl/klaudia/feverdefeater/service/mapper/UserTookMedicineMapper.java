package pl.klaudia.feverdefeater.service.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.view.dto.UsersTookMedicineDto;

@Component
@RequiredArgsConstructor
public class UserTookMedicineMapper {
    private final MedicineRepository medicineRepository;

    public UsersTookMedicineDto toUsersTookMedicineDto(TakeMedicine takeMedicine) {
        Medicine medicine = medicineRepository.findById(takeMedicine.getMedicineId()).orElse(null);
        return UsersTookMedicineDto.builder()
                .id(takeMedicine.getId())
                .medicineName(medicine.getName())
                .createdAt(takeMedicine.getCreatedAt())
                .medicineDoseMl(takeMedicine.getMedicineDoseMl())
                .timeOfNextDose(takeMedicine.getTimeOfNextDose())
                .timeWhenTookMedicine(takeMedicine.getTimeWhenTookMedicine())
                .user(takeMedicine.getUser())
                .build();
    }
}
