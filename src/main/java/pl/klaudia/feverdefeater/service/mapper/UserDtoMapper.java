package pl.klaudia.feverdefeater.service.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.klaudia.feverdefeater.controller.dto.CreateUserDto;
import pl.klaudia.feverdefeater.controller.dto.UserDto;
import pl.klaudia.feverdefeater.model.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserDtoMapper {


    public UserDto fromEntityToUserDto(User user) {
        List<Integer> tookMedicineIds = user.getTookMedicineList().stream()
                .map(takeMedicine -> takeMedicine.getId())
                .collect(Collectors.toList());

        return UserDto.builder()
                .id(user.getId())
                .createdAt(user.getCreatedAt())
                .login(user.getLogin())
                .role(user.getRole())
                .updatedAt(user.getUpdatedAt())
                .tookMedicineIdsList(tookMedicineIds)
                .build();
    }


    public User toEntity(CreateUserDto createUserDto){
        return User.builder()
                .login(createUserDto.getLogin())
                .password(createUserDto.getPassword())
                .role("USER")
                .createdAt(LocalDateTime.now())
                .build();
    }

}
