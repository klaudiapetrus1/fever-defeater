package pl.klaudia.feverdefeater.service.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.klaudia.feverdefeater.model.Child;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.repository.MedicineRepository;
import pl.klaudia.feverdefeater.service.MedicineService;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.view.dto.ChildDto;

@Component
@RequiredArgsConstructor
public class ChildDtoMapper {
    private final MedicineRepository medicineRepository;

    public Child toEntity(ChildDto dto) throws NotFound {
        Medicine medicine = medicineRepository.findById(dto.getMedicineId()).orElseThrow(NotFound::new);

        return Child.builder()
                .weight(dto.getWeight())
                .medicine(medicine)
                .build();
    }
}
