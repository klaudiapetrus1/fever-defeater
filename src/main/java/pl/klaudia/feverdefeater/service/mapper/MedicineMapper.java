package pl.klaudia.feverdefeater.service.mapper;

import org.springframework.stereotype.Component;
import pl.klaudia.feverdefeater.controller.dto.CreateMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.MedicineDto;
import pl.klaudia.feverdefeater.model.Medicine;

@Component
public class MedicineMapper {

    public Medicine createMedicineDtoToModel(CreateMedicineDto createMedicineDto){
        return Medicine.builder()
                .name(createMedicineDto.getName())
                .substance(createMedicineDto.getSubstance())
                .mgPerOneMl(createMedicineDto.getMgPerOneMl())
                .build();
    }

    public MedicineDto modelToMedicineDto(Medicine medicine){
        return MedicineDto.builder()
                .id(medicine.getId())
                .name(medicine.getName())
                .substance(medicine.getSubstance())
                .mgPerOneMl(medicine.getMgPerOneMl())
                .build();
    }
}
