package pl.klaudia.feverdefeater.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.klaudia.feverdefeater.controller.dto.CreateUserDto;
import pl.klaudia.feverdefeater.controller.dto.UserDto;
import pl.klaudia.feverdefeater.model.User;
import pl.klaudia.feverdefeater.repository.UserRepository;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.service.mapper.UserDtoMapper;
import pl.klaudia.feverdefeater.view.dto.UserEditDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserDtoMapper userDtoMapper;
    private final PasswordEncoder passwordEncoder;

    public List<UserDto> getUsers() {
        return userRepository.findAll().stream()
                .map(user -> userDtoMapper.fromEntityToUserDto(user))
                .collect(Collectors.toList());
    }

    public UserDto getUserById(Integer id) throws UserNotFound {
        return userDtoMapper.fromEntityToUserDto(userRepository.findById(id).orElseThrow(UserNotFound::new));
    }


    public UserDto createUser(CreateUserDto createUserDto) {
        User user = userDtoMapper.toEntity(createUserDto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        User saved = userRepository.save(user);
        return userDtoMapper.fromEntityToUserDto(saved);
    }


    public UserDto editUser(UserEditDto userEditDto) throws UserNotFound {
        User userToUpdate = userRepository.findById(userEditDto.getId()).orElseThrow(UserNotFound::new);
        userToUpdate.setLogin(userEditDto.getLogin());
        userToUpdate.setRole(userEditDto.getRole());

        User updated = userRepository.save(userToUpdate);
        return userDtoMapper.fromEntityToUserDto(updated);

    }



    public UserDto deleteUser(Integer id) throws UserNotFound {
        User userToBeDeleted = userRepository.findById(id).orElseThrow(UserNotFound::new);

        userRepository.delete(userToBeDeleted);

        return userDtoMapper.fromEntityToUserDto(userToBeDeleted);
    }

}
