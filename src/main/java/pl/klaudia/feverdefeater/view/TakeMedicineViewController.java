package pl.klaudia.feverdefeater.view;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.klaudia.feverdefeater.controller.TakeMedicineController;
import pl.klaudia.feverdefeater.controller.dto.CreateTakeMedicineDto;
import pl.klaudia.feverdefeater.model.Child;
import pl.klaudia.feverdefeater.model.MedicineDoseResult;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.model.User;
import pl.klaudia.feverdefeater.repository.UserRepository;
import pl.klaudia.feverdefeater.service.MedicineDoseService;
import pl.klaudia.feverdefeater.service.MedicineService;
import pl.klaudia.feverdefeater.service.TakeMedicineService;
import pl.klaudia.feverdefeater.service.exception.InvalidData;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.service.mapper.TakeMedicineMapper;
import pl.klaudia.feverdefeater.view.dto.ChildDto;
import pl.klaudia.feverdefeater.view.dto.MedicineApplicationDto;

import javax.servlet.http.HttpSession;

@Controller
@RequiredArgsConstructor
public class TakeMedicineViewController {

    private final TakeMedicineController takeMedicineController;
    private final TakeMedicineMapper takeMedicineMapper;
    private final UserRepository userRepository;


    @PostMapping("/apply-medicine")
    public String doTakeMedicine(@ModelAttribute(name = "medicineApplication") MedicineApplicationDto medicineApplicationDto, HttpSession httpSession, Authentication authentication) throws NotFound, UserNotFound {
        CreateTakeMedicineDto createTakeMedicineDto = takeMedicineMapper
                .fromMedicineApplicationDtoToCreateTakeMedicineDto(medicineApplicationDto);
        if(authentication !=  null) {
            Integer usersId = userRepository.findByLogin(authentication.getName()).get().getId();
            createTakeMedicineDto.setUsersId(usersId);
        }
        takeMedicineController.createTakeMedicine(createTakeMedicineDto, httpSession);

        return "redirect:/homepage";
    }
}
