package pl.klaudia.feverdefeater.view;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.klaudia.feverdefeater.controller.TakeMedicineController;
import pl.klaudia.feverdefeater.controller.UserController;
import pl.klaudia.feverdefeater.controller.dto.CreateUserDto;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.UserDto;
import pl.klaudia.feverdefeater.model.*;
import pl.klaudia.feverdefeater.service.MedicineDoseService;
import pl.klaudia.feverdefeater.service.MedicineService;
import pl.klaudia.feverdefeater.service.UserService;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.mapper.ChildDtoMapper;
import pl.klaudia.feverdefeater.service.mapper.MedicineApplicationMapper;
import pl.klaudia.feverdefeater.service.mapper.TakeMedicineViewDtoMapper;
import pl.klaudia.feverdefeater.view.dto.ChildDto;
import pl.klaudia.feverdefeater.view.dto.TakeMedicineViewDto;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class HomepageViewController {
    private final MedicineService service;
    private final MedicineDoseService doseService;
    private final ChildDtoMapper childDtoMapper;
    private final TakeMedicineController takeMedicineController;
    private final MedicineApplicationMapper medicineApplicationMapper;
    private final TakeMedicineViewDtoMapper takeMedicineViewDtoMapper;
    private final UserService userService;

    @GetMapping("/homepage")
    public ModelAndView getHomepage(Model model, HttpSession httpSession) {
        List<Integer> takeMedicinesIdsTmp = (List<Integer>) httpSession.getAttribute("take-medicines");
        if (takeMedicinesIdsTmp == null) {
            takeMedicinesIdsTmp = new ArrayList<>();
        }
        List<Integer> takeMedicinesIds = takeMedicinesIdsTmp;

        ChildDto child = new ChildDto();
        List<TakeMedicineViewDto> takeMedicineViewDtoList = takeMedicineController.getAll().stream()
                .map(takeMedicineDto -> takeMedicineViewDtoMapper.fromTakeMedicineDtoToView(takeMedicineDto))
                .filter(tm -> takeMedicinesIds.contains(tm.getId()))
                .collect(Collectors.toList());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        model.addAttribute("formatter", formatter);
        model.addAttribute("takeMedicineViewDtoList", takeMedicineViewDtoList);
        model.addAttribute("child", child);
        model.addAttribute("medicineList", service.getAllMedicines());

        return new ModelAndView("homepage");
    }

    @PostMapping("/homepage")
    public ModelAndView showCalculations(@ModelAttribute(name = "child") ChildDto childDto,
                                         @ModelAttribute(name = "takeMedicine") TakeMedicine takeMedicine) throws NotFound {

        Child child = childDtoMapper.toEntity(childDto);
        MedicineDoseResult medicineDoseResult = doseService.calculateMedicineDose(child);

        ModelAndView modelAndView = new ModelAndView("homepage");

        modelAndView.addObject("takeMedicine", takeMedicine);
        modelAndView.addObject("medicineDoseResult", medicineDoseResult);
        modelAndView.addObject("medicineList", service.getAllMedicines());
        modelAndView.addObject("medicineApplication",
                medicineApplicationMapper.toDto(childDto, medicineDoseResult));
        return modelAndView;
    }

    @GetMapping("/register")
    public ModelAndView getRegisterForm(Model model) {
        CreateUserDto createUserDto = new CreateUserDto();
        model.addAttribute("createUserDto", createUserDto);
        return new ModelAndView("register-form");
    }

    @PostMapping("/register")
    public String createNewUserFromRegistrationForm(@Valid @ModelAttribute(name = "createUserDto") CreateUserDto createUserDto,
                                                    BindingResult bindingResult, Model model) {

        if (createUserDto.getRepeatedPassword().equals(createUserDto.getPassword()) && !bindingResult.hasErrors()) {
            UserDto createdUserDto = userService.createUser(createUserDto);
            model.addAttribute("createdUser", createdUserDto);
            return "succeed-registration";

        }
        if (!createUserDto.getRepeatedPassword().equals(createUserDto.getPassword())){
            bindingResult.addError(new ObjectError("repeatedPassword", "Hasło musi być takie samo!"));
        }

        return "register-form";

    }
}
