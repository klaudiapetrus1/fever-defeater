package pl.klaudia.feverdefeater.view;

import com.google.common.hash.Hasher;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.klaudia.feverdefeater.controller.dto.ChangePasswordUserDto;
import pl.klaudia.feverdefeater.model.User;
import pl.klaudia.feverdefeater.repository.UserRepository;
import pl.klaudia.feverdefeater.service.UserService;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.service.mapper.UserTookMedicineMapper;
import pl.klaudia.feverdefeater.view.dto.UsersTookMedicineDto;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class MyAccountViewController {
    private final UserRepository userRepository;
    private final UserTookMedicineMapper userTookMedicineMapper;
    private final PasswordEncoder passwordEncoder;

    @GetMapping("/my-account")
    public ModelAndView getMyAcoount(){
        return new ModelAndView("my-account");
    }

    @GetMapping("/user-details")
    public ModelAndView getUserDetails(Model model, Authentication authentication) throws UserNotFound {
        User user = userRepository.findByLogin(authentication.getName()).orElseThrow(UserNotFound::new);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
        model.addAttribute("formatter", formatter);
        model.addAttribute("user", user);
        return new ModelAndView("user-details");
    }

    @GetMapping("/my-took-medicine-list")
    public ModelAndView getUsersTakeMedicineList(Authentication authentication) throws UserNotFound {
        ModelAndView modelAndView = new ModelAndView("my-took-medicine-list");
        User user = userRepository.findByLogin(authentication.getName()).orElseThrow(UserNotFound::new);
        List<UsersTookMedicineDto> usersTookMedicineDtoList = user.getTookMedicineList().stream()
                .map(takeMedicine -> userTookMedicineMapper.toUsersTookMedicineDto(takeMedicine))
                .collect(Collectors.toList());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
        modelAndView.addObject("formatter", formatter);
        modelAndView.addObject("user", user);
        modelAndView.addObject("usersTookMedicineDtoList", usersTookMedicineDtoList);

        return modelAndView;
    }

    @GetMapping("/change-password")
    public ModelAndView getChangePasswordForm(Model model){
        model.addAttribute("changePasswordUserDto", new ChangePasswordUserDto());
        return new ModelAndView("change-password-form");
    }

    @PostMapping("/change-password")
    public ModelAndView changeUsersPassword(@ModelAttribute(name = "changePasswordUserDto") ChangePasswordUserDto changePasswordUserDto,
                                            Authentication authentication) throws UserNotFound {
        User user = userRepository.findByLogin(authentication.getName()).orElseThrow(UserNotFound::new);

        if(changePasswordUserDto.getNewPassword().equals(changePasswordUserDto.getRepeatedNewPassword())){
            if(passwordEncoder.matches(changePasswordUserDto.getPassword(), user.getPassword())){
                user.setPassword(passwordEncoder.encode(changePasswordUserDto.getNewPassword()));
                userRepository.save(user);
                return new ModelAndView("change-password-succeed");
            }
        }

        return new ModelAndView("change-password-form");
    }

}
