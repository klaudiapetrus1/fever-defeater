package pl.klaudia.feverdefeater.view;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.klaudia.feverdefeater.controller.dto.CreateMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.MedicineDto;
import pl.klaudia.feverdefeater.controller.dto.UserDto;
import pl.klaudia.feverdefeater.model.Substance;
import pl.klaudia.feverdefeater.model.User;
import pl.klaudia.feverdefeater.repository.TakeMedicineRepository;
import pl.klaudia.feverdefeater.repository.UserRepository;
import pl.klaudia.feverdefeater.service.MedicineService;
import pl.klaudia.feverdefeater.service.UserService;
import pl.klaudia.feverdefeater.service.exception.InvalidData;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.service.mapper.UserDtoMapper;
import pl.klaudia.feverdefeater.service.mapper.UserTookMedicineMapper;
import pl.klaudia.feverdefeater.view.dto.UserEditDto;
import pl.klaudia.feverdefeater.view.dto.UsersTookMedicineDto;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminPanelViewController {
    private final UserService userService;
    private final UserRepository userRepository;
    private final UserTookMedicineMapper userTookMedicineMapper;
    private final TakeMedicineRepository takeMedicineRepository;
    private final MedicineService medicineService;
    private final UserDtoMapper userDtoMapper;


    @GetMapping("/admin-panel")
    public ModelAndView getAdminPanel(){
        return new ModelAndView("admin-panel");
    }

    @GetMapping("/users-list")
    public ModelAndView getUsersList(Model model){
        model.addAttribute("usersList", userService.getUsers());
        return new ModelAndView("users-list");
    }

    @GetMapping("/delete-user/{id}")
    public ModelAndView deleteUser(@PathVariable Integer id, Model model) throws UserNotFound {
        UserDto userDto = userService.getUserById(id);
        model.addAttribute("userToBeDeleted", userDto);

        return new ModelAndView("user-delete-form");
    }

    @PostMapping("/delete-user")
    public ModelAndView deleteUserFromForm(@ModelAttribute(name ="userToBeDeleted") UserDto userToBeDeleted) throws UserNotFound {
        userService.deleteUser(userToBeDeleted.getId());

        return new ModelAndView("users-list");
    }

    @GetMapping("/edit-user/{id}")
    public ModelAndView editUser(@PathVariable Integer id, Model model) throws UserNotFound {
        UserDto userDto = userService.getUserById(id);
        model.addAttribute("userToBeEdited", userDto);
        model.addAttribute("usersNewData", new UserEditDto());

        return new ModelAndView("user-edit-form");
    }

    @PostMapping("/edit-user")
    public String editUserFromForm(@ModelAttribute(name = "usersNewData") UserEditDto usersNewData) throws UserNotFound {
     UserDto saved = userService.editUser(usersNewData);

        return "redirect:/admin/users-list";
    }

    @GetMapping("/users-tookMedicineList/{id}")
    public ModelAndView showUsersTookMedicineList(@PathVariable Integer id) throws UserNotFound {
        ModelAndView modelAndView = new ModelAndView("users-takeMedicine-list");
        User user = userRepository.findById(id).orElseThrow(UserNotFound::new);
        List<UsersTookMedicineDto> usersTakeMedicineList = user.getTookMedicineList().stream()
                .map(takeMedicine -> userTookMedicineMapper.toUsersTookMedicineDto(takeMedicine)).collect(Collectors.toList());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
        modelAndView.addObject("formatter", formatter);
        modelAndView.addObject("user", user);
        modelAndView.addObject("usersTakeMedicineList", usersTakeMedicineList);
        return modelAndView;
    }

    @GetMapping("/tookMedicine-list")
    public ModelAndView getAllTookMedicineList(){
        ModelAndView modelAndView = new ModelAndView("tookMedicine-list");
        List<UsersTookMedicineDto> usersTookMedicineDtoList = takeMedicineRepository.findAll().stream()
                                                .map(takeMedicine -> userTookMedicineMapper
                                                .toUsersTookMedicineDto(takeMedicine))
                                                .collect(Collectors.toList());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
        modelAndView.addObject("formatter", formatter);
        modelAndView.addObject("tookMedicineList", usersTookMedicineDtoList);
        return modelAndView;
    }

    @GetMapping("/medicine-list")
    public ModelAndView getMedicineList(){
        ModelAndView modelAndView = new ModelAndView("medicine-list");
        modelAndView.addObject("medicineList", medicineService.getAllMedicines());
        return modelAndView;
    }

    @GetMapping("/add-medicine-form")
    public ModelAndView getMedicineForm(Model model){

        model.addAttribute("createMedicineDto", new CreateMedicineDto());
        return new ModelAndView("add-medicine-form");
    }


    @PostMapping("/add-medicine")
    public String addNewMedicine(@ModelAttribute(name = "createMedicineDto") @Valid CreateMedicineDto createMedicineDto){

        medicineService.addMedicine(createMedicineDto);

        return "redirect:/admin/medicine-list";
    }

    @GetMapping("/delete-medicine/{id}")
    public ModelAndView getMedicineDeleteForm(@PathVariable Integer id, Model model) throws UserNotFound, InvalidData {
        MedicineDto medicineDto = medicineService.findMedicineById(id);
        model.addAttribute("medicineToBeDeleted", medicineDto);

        return new ModelAndView("medicine-delete-form");
    }

    @PostMapping("/delete-medicine")
    public String deleteMedicine(@ModelAttribute(name = "medicineToBeDeleted") MedicineDto medicineDto) throws InvalidData {
        medicineService.deleteMedicineById(medicineDto.getId());

        return "redirect:/admin/medicine-list";
    }

    @GetMapping("/edit-medicine/{id}")
    public ModelAndView getMedicineEditForm(Model model, @PathVariable Integer id) throws InvalidData {
        model.addAttribute("medicineToBeEdited", medicineService.findMedicineById(id));
        model.addAttribute("updatedMedicine", new CreateMedicineDto());
        return new ModelAndView("medicine-edit-form");
    }

    @PostMapping("/edit-medicine/{id}")
    public String editMedicineFromForm(@ModelAttribute(name = "updatedMedicine") CreateMedicineDto createMedicineDto,
                                             @PathVariable Integer id) throws InvalidData {
        medicineService.updateMedicine(id,createMedicineDto);

        return "redirect:/admin/medicine-list";
    }


}
