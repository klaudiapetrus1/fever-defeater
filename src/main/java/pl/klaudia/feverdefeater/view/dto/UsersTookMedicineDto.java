package pl.klaudia.feverdefeater.view.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.model.User;

import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsersTookMedicineDto {
    private Integer id;
    private String medicineName;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime timeWhenTookMedicine;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime timeOfNextDose;
    private double medicineDoseMl;
    private OffsetDateTime createdAt;
    private User user;
}
