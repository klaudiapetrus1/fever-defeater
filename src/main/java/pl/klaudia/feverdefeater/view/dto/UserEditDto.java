package pl.klaudia.feverdefeater.view.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEditDto {
    @NotBlank
    private Integer id;
    @NotBlank
    private String login;
    @NotBlank
    private String role;
}
