package pl.klaudia.feverdefeater.view.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class MedicineApplicationDto {
    private double weight;
    private int medicineId;
    private double singleMedicineDoseMg;
    private double singleMedicineDoseMl;
    private double dailyMedicineDoseMl;
    private double dailySubstanceDoseMg;
    private int numberOfDailyDoses;
}
