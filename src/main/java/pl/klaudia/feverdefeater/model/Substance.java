package pl.klaudia.feverdefeater.model;



public enum Substance {
    IBUPROFEN("Ibuprofen"),
    PARACETAMOL("Paracetamol");

    private final String displayName;

    Substance(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
