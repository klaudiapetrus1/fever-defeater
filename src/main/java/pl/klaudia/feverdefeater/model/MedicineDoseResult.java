package pl.klaudia.feverdefeater.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MedicineDoseResult {

    private double singleMedicineDoseMg;
    private double singleMedicineDoseMl;
    private double dailyMedicineDoseMl;
    private double dailySubstanceDoseMg;
    private int numberOfDailyDoses;
    private Substance substance;
}
