package pl.klaudia.feverdefeater.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.klaudia.feverdefeater.service.exception.AlReadyExists;
import pl.klaudia.feverdefeater.service.exception.InvalidData;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;

@ControllerAdvice
public class ControllerConfig {

    @ExceptionHandler(InvalidData.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleDataInvalid() {
    }

    @ExceptionHandler(NotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleNotFound() {
    }

    @ExceptionHandler(AlReadyExists.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public void handleAlreadyExists() {
    }

    @ExceptionHandler(UserNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleUserNotFound(){
    }

}
