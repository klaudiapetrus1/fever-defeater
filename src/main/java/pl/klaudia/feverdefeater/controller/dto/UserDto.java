package pl.klaudia.feverdefeater.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.klaudia.feverdefeater.model.TakeMedicine;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private Integer id;
    private String login;
    private String role;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private List<Integer> tookMedicineIdsList;

}
