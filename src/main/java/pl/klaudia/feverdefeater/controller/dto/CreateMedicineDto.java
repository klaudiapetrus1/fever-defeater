package pl.klaudia.feverdefeater.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.klaudia.feverdefeater.model.Substance;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateMedicineDto {
    @NotNull
    private String name;
    @NotNull
    private Substance substance;
    @NotNull
    private double mgPerOneMl;
}
