package pl.klaudia.feverdefeater.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.klaudia.feverdefeater.model.Substance;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MedicineDto {
    private Integer id;
    private String name;
    private Substance substance;
    private double mgPerOneMl;
}
