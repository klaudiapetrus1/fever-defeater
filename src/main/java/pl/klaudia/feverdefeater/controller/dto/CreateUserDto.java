package pl.klaudia.feverdefeater.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserDto {
    @NotNull(message = "Pole login nie może być puste!")
    @Size(min = 3, message = "Hasło musi być dłuższe niż 3 znaki!")
    private String login;
    @NotNull(message = "Hasło nie może być puste!")
    @Size(min = 3, message = "Hasło musi być dłuższe niż 3 znaki!")
    private String password;
    private String repeatedPassword;
}
