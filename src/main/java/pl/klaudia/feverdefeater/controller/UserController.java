package pl.klaudia.feverdefeater.controller;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.klaudia.feverdefeater.controller.dto.CreateUserDto;
import pl.klaudia.feverdefeater.controller.dto.ChangePasswordUserDto;
import pl.klaudia.feverdefeater.controller.dto.UserDto;
import pl.klaudia.feverdefeater.model.User;
import pl.klaudia.feverdefeater.service.UserService;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.view.dto.UserEditDto;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @GetMapping
    public List<UserDto> getUsers(){
        return userService.getUsers();
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable Integer id) throws UserNotFound {
        return userService.getUserById(id);
    }

    @PostMapping
    public UserDto createUser(@RequestBody @Valid CreateUserDto createUserDto){
        return userService.createUser(createUserDto);
    }

    @PutMapping
    public UserDto editUser(@RequestBody @Valid UserEditDto userEditDto) throws UserNotFound {
       return userService.editUser(userEditDto);

    }

    @DeleteMapping("/{id}")
    public UserDto deleteUserById(@PathVariable Integer id) throws UserNotFound {
        return userService.deleteUser(id);
    }
}
