package pl.klaudia.feverdefeater.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.klaudia.feverdefeater.controller.dto.CreateTakeMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.TakeMedicineDto;
import pl.klaudia.feverdefeater.model.TakeMedicine;
import pl.klaudia.feverdefeater.service.TakeMedicineService;
import pl.klaudia.feverdefeater.service.exception.NotFound;
import pl.klaudia.feverdefeater.service.exception.UserNotFound;
import pl.klaudia.feverdefeater.service.mapper.TakeMedicineMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@Validated
@RestController
@RequestMapping("/take-medicine")
@RequiredArgsConstructor
public class TakeMedicineController {
    private final TakeMedicineMapper takeMedicineMapper;
    private final TakeMedicineService takeMedicineService;

    @GetMapping
    public List<TakeMedicineDto> getAll(){
        return takeMedicineService.getAll();
    }

    @GetMapping("/{id}")
    public TakeMedicineDto findTakeMedicineById(@PathVariable int id) throws NotFound {
        return takeMedicineService.findTakeMedicineById(id);
    }

    @PostMapping("/create-take-medicine")
    public TakeMedicineDto createTakeMedicine(@Valid @RequestBody CreateTakeMedicineDto createTakeMedicineDto, HttpSession httpSession) throws UserNotFound {
        TakeMedicineDto takeMedicine = takeMedicineService.createTakeMedicine(createTakeMedicineDto);
        List<Integer> takeMedicinesIds = (List<Integer>) httpSession.getAttribute("take-medicines");
        httpSession.setMaxInactiveInterval(60*60*12);
        if(takeMedicinesIds == null){
            takeMedicinesIds = new ArrayList<>();
        }

        takeMedicinesIds.add(takeMedicine.getId());
        httpSession.setAttribute("take-medicines", takeMedicinesIds);

        return takeMedicine;
    }
    @DeleteMapping("/{id}")
    public TakeMedicineDto deleteTakeMedicine(@PathVariable int id) throws NotFound {
        return takeMedicineService.deleteTakeMedicine(id);

    }

}
