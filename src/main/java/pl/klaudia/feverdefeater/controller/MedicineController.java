package pl.klaudia.feverdefeater.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.klaudia.feverdefeater.controller.dto.CreateMedicineDto;
import pl.klaudia.feverdefeater.controller.dto.MedicineDto;
import pl.klaudia.feverdefeater.model.Medicine;
import pl.klaudia.feverdefeater.service.MedicineService;
import pl.klaudia.feverdefeater.service.exception.InvalidData;
import pl.klaudia.feverdefeater.service.mapper.MedicineMapper;

import javax.validation.Valid;
import java.util.List;
@Validated
@RestController
@RequestMapping("/medicine")
@RequiredArgsConstructor
public class MedicineController {

    private final MedicineService medicineService;
    private final MedicineMapper mapper;

    @GetMapping
    public List<MedicineDto> getAllMedicines(){
        return medicineService.getAllMedicines();
    }

    @GetMapping("/{id}")
    public MedicineDto findMedicineById(@PathVariable int id) throws InvalidData {
        return medicineService.findMedicineById(id);
    }

    @PostMapping("/add-new-medicine")
    public MedicineDto addNewMedicine(@Valid @RequestBody CreateMedicineDto createMedicineDto){
        medicineService.addMedicine(createMedicineDto);
        return mapper.modelToMedicineDto(mapper.createMedicineDtoToModel(createMedicineDto));
    }

    @DeleteMapping("/{id}")
    public MedicineDto deleteMedicineById(@PathVariable int id) throws InvalidData {
        return medicineService.deleteMedicineById(id);
    }

    @PutMapping("/{id}")
    public MedicineDto updateMedicine(@PathVariable int id, @Valid @RequestBody CreateMedicineDto createMedicineDto) throws InvalidData {
        return medicineService.updateMedicine(id, createMedicineDto);
    }
}
