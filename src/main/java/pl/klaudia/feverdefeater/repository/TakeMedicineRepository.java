package pl.klaudia.feverdefeater.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.klaudia.feverdefeater.model.TakeMedicine;

@Repository
public interface TakeMedicineRepository extends JpaRepository<TakeMedicine, Integer> {
}
