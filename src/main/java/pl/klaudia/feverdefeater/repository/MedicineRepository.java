package pl.klaudia.feverdefeater.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.klaudia.feverdefeater.model.Medicine;
@Repository
public interface MedicineRepository extends JpaRepository<Medicine, Integer> {
}
