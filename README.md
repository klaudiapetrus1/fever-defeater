![Users Panel](./img/logo.png)
# Fever Defeater
This Spring application supports parents in dosage of fever syrups for children. Based on user’s input the application returns the recommended dose of a specific medicine and also counts down to the next dose accordingly.

You can check it out on http://fever-defeater.pl/homepage


## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)

## General info
Fever Defeater also has an admin panel to manage users (can delete or modify users data and permissions, and also is able to see users medications taken history) and medicines (able to add new medicine, delete or edit it).

For registered users the application provides special panel which allows them to see their history of medications taken, see their data and also change their account password.

## Screenshots

*Panel for registered users:*

![Users Panel](./img/users-panel.png)

*Admin panel:*

![Users Panel](./img/admin-panel.png)


## Technologies
* Spring Framework
* Thymeleaf
* Junit 4
* Hibernate
* Maven
* Mockito

__Also used__
* Java 11
* Mysql
* Lombok
* Swagger


## Features
List of features ready and TODOs for future development
* Registration form for users
* Password change feature
* Additional count down clock feature in JavaScript

To-do list:
* Organize stylesheets better. Remove all additional css styles from html files to external css file
* Improve the responsiveness of the website

## Status
Project is: basically finished, but needs some improvements as listed above.

## Contact
Created by [Klaudia Petrus](https://www.linkedin.com/in/klaudiapetrus/) - feel free to contact me on linkedin!
